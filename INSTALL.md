## Installation et démarrage du projet

### Installation
Pour faire tourner le projet sur votre poste vous aurez besoin de :
* git
* make
* docker-compose (min 1.23)

En dehors du pull du projet la seule étape que vous aurez à réaliser est la configuration du `/etc/hosts`.
Ajoutez-y la ligne 
`127.0.0.1       orion-test.igraal.com`

### Démarrage du projet
Une fois le projet cloné en local sur votre poste, mettez-vous à la racine de celui-ci et faites un `make start_working`.

Cette commande va initialiser les configurations du projet, installer les dépendances et créer la base de données.

### Troubleshooting
En cas d'erreur de network, exécutez la commande :
`docker network create igraal-orion-test`

<?php

namespace App\Tools;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class Serializer
{
    private $serializer;

    public function __construct()
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $objectNormalizer = new ObjectNormalizer($classMetadataFactory, null, null, new ReflectionExtractor());
        $dateTimeNormalizer = new DateTimeNormalizer();
        $this->serializer = new \Symfony\Component\Serializer\Serializer(
            [$dateTimeNormalizer, $objectNormalizer, new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
    }

    public function normalize($data, string $group): array
    {
        return $this->serializer->normalize($data, null, ['groups' => [$group]]);
    }
}
<?php

namespace App\Event;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

final class ApiRequestEventListener
{
    public function convertResponseToHtml(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $path = $request->getPathInfo();

        if (false === $event->isMasterRequest()
            || 0 !== strpos($path, '/api')
            || Response::HTTP_OK !== $event->getResponse()->getStatusCode()
        ) {
            return;
        }

        if (false === $request->headers->has('Accept') || false === strpos($request->headers->get('Accept'), 'text/html')) {
            return;
        }

        $response = $event->getResponse();
        $content = json_encode(json_decode($response->getContent()), JSON_PRETTY_PRINT);
        $response->setContent('<html><body><pre><code>'.htmlspecialchars($content).'</code></pre></body></html>');

        $response->headers->set('Content-Type', 'text/html; charset=UTF-8');
        $request->setRequestFormat('html');
        $event->setResponse($response);
    }
}
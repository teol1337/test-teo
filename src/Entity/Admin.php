<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminRepository")
 * @ORM\Table(name="ADMIN")
 */
class Admin
{
    /**
     * @var int
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="status", type="string", nullable=false, unique=true)
     */
    private $name;

    /**
     * @var Merchant[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Merchant", mappedBy="admin")
     */
    private $merchants;

    public function __construct()
    {
        $this->merchants = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Merchant[]
     */
    public function getMerchants(): array
    {
        if ($this->merchants instanceof PersistentCollection) {
            return $this->merchants->toArray();
        }

        return $this->merchants;
    }

    /**
     * @param Merchant[] $merchants
     */
    public function setMerchants(array $merchants): void
    {
        $this->merchants = $merchants;
    }
}
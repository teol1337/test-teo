<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoucherRepository")
 * @ORM\Table(name="VOUCHER")
 */
class Voucher
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_ACTIVE = 'active';

    /**
     * @var int
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="title", type="string", nullable=false, unique=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="description", type="string", nullable=true, unique=false)
     */
    private $description;

    /**
     * @var float
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="value", type="float", nullable=false, unique=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, unique=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="dateStart", type="datetime", nullable=false, unique=false)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="dateEnd", type="datetime", nullable=false, unique=false)
     */
    private $dateEnd;

    /**
     * @var string|null
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="admin_note", type="string", nullable=true, unique=false)
     */
    private $adminNote;

    /**
     * @var int
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="rank", type="integer", nullable=false, unique=true)
     */
    private $rank;

    /**
     * @var string
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="status", type="string", nullable=false, unique=false)
     */
    private $status = self::STATUS_PENDING;

    /**
     * @var bool
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="is_public", type="boolean", nullable=false, unique=false)
     */
    private $isPublic = 0;

    /**
     * @var Merchant
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Merchant", inversedBy="vouchers")
     * @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     */
    private $merchant;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue(float $value): void
    {
        $this->value = $value;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    public function getDateStart(): \DateTime
    {
        return $this->dateStart;
    }

    /**
     * @param \DateTime $dateStart
     */
    public function setDateStart(\DateTime $dateStart): void
    {
        $this->dateStart = $dateStart;
    }

    public function getDateEnd(): \DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @param \DateTime $dateEnd
     */
    public function setDateEnd(\DateTime $dateEnd): void
    {
        $this->dateEnd = $dateEnd;
    }

    public function getAdminNote(): ?string
    {
        return $this->adminNote;
    }

    /**
     * @param string|null $adminNote
     */
    public function setAdminNote(?string $adminNote): void
    {
        $this->adminNote = $adminNote;
    }

    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     */
    public function setRank(int $rank): void
    {
        $this->rank = $rank;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function isPublic(): bool
    {
        return $this->isPublic;
    }

    /**
     * @param bool $isPublic
     */
    public function setIsPublic(bool $isPublic): void
    {
        $this->isPublic = $isPublic;
    }

    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    /**
     * @param Merchant $merchant
     */
    public function setMerchant(Merchant $merchant): void
    {
        $this->merchant = $merchant;
    }
}
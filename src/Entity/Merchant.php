<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MerchantRepository")
 * @ORM\Table(name="MERCHANT")
 */
class Merchant
{
    /**
     * @var int
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="url_name", type="string", nullable=false, unique=true)
     */
    private $urlName;

    /**
     * @var string|null
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="description", type="text", nullable=true, unique=false)
     */
    private $description;

    /**
     * @var float|null
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="cashback_fixed_value", type="float", nullable=true, unique=false)
     */
    private $cashbackFixedValue;

    /**
     * @var float|null
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="cashback_percent_value", type="float", nullable=true, unique=false)
     */
    private $cashbackPercentValue;

    /**
     * @var \DateTimeInterface
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false, unique=false)
     */
    private $creationDate;

    /**
     * @var string|null
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\Column(name="admin_note", type="string", nullable=true, unique=false)
     */
    private $adminNote;

    /**
     * @var Voucher[]
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Voucher", mappedBy="merchant")
     */
    private $vouchers;

    /**
     * @var Admin
     *
     * @Serializer\Groups({"admin", "public"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin", inversedBy="merchants")
     * @ORM\JoinColumn(name="admin_id", referencedColumnName="id")
     */
    private $admin;

    /**
     * @var string
     *
     * @Serializer\Groups({"admin", "public"})
     */
    private $cashbackLabel;

    /**
     * @var string
     *
     * @Serializer\Groups({"admin", "public"})
     */
    private $voucherLabel;

    public function __construct()
    {
        $this->creationDate = new \DateTime();
        $this->vouchers = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getVoucherLabel(): string
    {
        return $this->voucherLabel;
    }

    public function setVoucherLabel(string $voucherLabel): void
    {
        $this->voucherLabel = $voucherLabel;
    }

    public function getCashbackLabel(): ?string
    {
        return $this->cashbackLabel;
    }

    public function setCashbackLabel(?string $cashbackLabel): void
    {
        $this->cashbackLabel = $cashbackLabel;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getUrlName(): string
    {
        return $this->urlName;
    }

    /**
     * @param string $urlName
     */
    public function setUrlName(string $urlName): void
    {
        $this->urlName = $urlName;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getCashbackFixedValue(): ?float
    {
        return $this->cashbackFixedValue;
    }

    /**
     * @param float|null $cashbackFixedValue
     */
    public function setCashbackFixedValue(?float $cashbackFixedValue): void
    {
        $this->cashbackFixedValue = $cashbackFixedValue;
    }

    public function getCashbackPercentValue(): ?float
    {
        return $this->cashbackPercentValue;
    }

    /**
     * @param float|null $cashbackPercentValue
     */
    public function setCashbackPercentValue(?float $cashbackPercentValue): void
    {
        $this->cashbackPercentValue = $cashbackPercentValue;
    }

    public function getCreationDate(): \DateTimeInterface
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTimeInterface $creationDate
     */
    public function setCreationDate(\DateTimeInterface $creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    public function getAdminNote(): ?string
    {
        return $this->adminNote;
    }

    /**
     * @param string|null $adminNote
     */
    public function setAdminNote(?string $adminNote): void
    {
        $this->adminNote = $adminNote;
    }

    /**
     * @return Voucher[]
     */
    public function getVouchers(): array
    {
        if ($this->vouchers instanceof PersistentCollection) {
            return $this->vouchers->toArray();
        }

        return $this->vouchers;
    }

    /**
     * @param Voucher[] $vouchers
     */
    public function setVouchers(array $vouchers): void
    {
        $this->vouchers = $vouchers;
    }

    public function getAdmin(): Admin
    {
        return $this->admin;
    }

    /**
     * @param Admin $admin
     */
    public function setAdmin(Admin $admin): void
    {
        $this->admin = $admin;
    }
}
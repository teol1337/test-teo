<?php

namespace App\Controller;

use App\Entity\Merchant;
use App\Repository\MerchantRepository;
use App\Tools\Serializer;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class MerchantController extends AbstractController
{
    /**
     * @Route(name="merchants_publicèwebsite", path="/api/merchants", methods={"GET"})
     */
    public function findForPublicWebsite(
        EntityManagerInterface $entityManager
    ): JsonResponse
    {
        /** @var Merchant[] $merchants */
        $merchants = $this->getMerchantRepository($entityManager)->findMerchants();

        //Faire ça directement en SQL ou dans l'entité
        foreach ($merchants as $merchant) {
            if (0 === count($merchant->getVouchers())) {
                $merchant->setVoucherLabel('Triste !');
            } elseif (count($merchant->getVouchers()) >= 1 && count($merchant->getVouchers()) < 3) {
                $merchant->setVoucherLabel('Pas mal !');
            } else {
                $merchant->setVoucherLabel('Incroyable !');
            }
        }

        //Utiliser le serializer du projet pour éviter de réécrire du code
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $objectNormalizer = new ObjectNormalizer($classMetadataFactory, null, null, new ReflectionExtractor());
        $dateTimeNormalizer = new DateTimeNormalizer();
        $serializer = new \Symfony\Component\Serializer\Serializer(
            [$dateTimeNormalizer, $objectNormalizer, new ArrayDenormalizer()],
            [new JsonEncoder()]
        );

        $filteredMerchants = [];
        foreach ($merchants as $merchant) {
            //Faire en SQL directement
            $merchant->setName(strtoupper($merchant->getName()));
            if (null !== $merchant->getCashbackPercentValue() && null === $merchant->getCashbackFixedValue()) {
                $merchant->setCashbackLabel($merchant->getCashbackPercentValue() . '%');
            }

            //Faire dans l'entité directement
            if (null !== $merchant->getCashbackFixedValue() && null === $merchant->getCashbackPercentValue()) {
                $merchant->setCashbackLabel($merchant->getCashbackFixedValue() . ' euros');
            }

            //à faire en SQL directement
            foreach ($merchant->getVouchers() as $voucher) {
                $voucher->setTitle(strtoupper($voucher->getTitle()));
            }
            
            //Faire en SQL directement
            if (null !== $merchant->getCashbackLabel()) {
                $filteredMerchants [] = $merchant;
            } else {
                //Faire cette action d'une autre manière, mais pas dans ce controlleur
                $this->sendEmailAlertToAdmin($merchant);
            }
        }

        $result = $serializer->normalize($filteredMerchants, null, ['groups' => ['public']]);

        return new JsonResponse($result);
    }

    /**
     * @return MerchantRepository|ObjectRepository
     */
    protected function getMerchantRepository(EntityManagerInterface $entityManager): ObjectRepository
    {
        return $entityManager->getRepository(Merchant::class);
    }

    protected function sendEmailAlertToAdmin(Merchant $merchant): void
    {
        // This mock method simulates the send of an email to the Merchant Admin reporting the missing of information.
        // The sleep is here to simulate the time require to build and send each email.
        // You can comment the sleep when you suggest a more optimized solution to handle this functionality (if it requires a new technology just put it's name in a comment).
        //usleep(50000);
    }
}
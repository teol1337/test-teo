#Setup automatically docker compose variables
include .env
-include .env.local

PHP := docker-compose exec php
COMPOSER := $(PHP) composer
COMPOSE=docker-compose
DOCKER_COMPOSE_EXEC := $(COMPOSE) exec

.env.local:
	@touch .env.local

docker-compose.yaml: docker-compose.yaml.dist
	@cp $< $@
	@sed -i "s/<DOCKER_USER_ID>/$(shell $(shell echo id -u ${USER}))/g" $@
	@sed -i "s/<DOCKER_USER>/$(shell echo ${USER})/g" $@
	@sed -i 's/<REMOTE_HOST>/$(shell hostname -I | grep -Eo "192\.168\.[0-9]{,2}\.[0-9]+" | head -1)/g' $@

setup: docker-compose.yaml .env.local

down: setup
	$(COMPOSE) down  -v --rmi all;

up: down
	docker-compose pull --ignore-pull-failures --quiet &>/dev/null; \
	docker-compose up -d --remove-orphans;

vendor: up
	@$(COMPOSER) install

start_working: vendor
	sleep 10
	make create_db

create_db:
	@$(PHP) bin/console doctrine:database:drop --force --if-exists --env=$(APP_ENV)
	@$(PHP) bin/console doctrine:database:create --env=$(APP_ENV)
	@$(PHP) bin/console doctrine:schema:create
	$(PHP) bin/console hautelook:fixtures:load --purge-with-truncate --no-interaction

mysql.connect:
	@$(DOCKER_COMPOSE_EXEC) mysql /bin/bash -c 'mysql -u$$MYSQL_USER -p$$MYSQL_PASSWORD $$MYSQL_DB_NAME'


## Démarrage du projet sur votre poste
Si celui-ci n'est pas installé, consultez la procédure dans le fichier [INSTALL.md](INSTALL.md).

Pour démarrer le projet, lancez la commande `make start_working` depuis la racine

## Votre mission si vous l'acceptez
L'api `/api/merchants` est à destination des applications mobile de nos clients.
Problème, celle-ci est très lente compte tenu du volume de données qui y sera exposé à terme.
De plus les merchants et les vouchers affichés ne semblent pas correctement filtrés. 
L'interface mobile va afficher :
* le nom du merchant
* un texte en fonction du nombre de voucher via le champs voucherLabel
* la valeur de son cashback via le champs cashbackLabel
* un lien de navigation de la forme http://igraal.com/<merchant-url-name>
* la liste de ses vouchers avec pour chacun d'entre eux :
    * le nom du voucher
    * sa valeur
    * sa date de fin

Après discussion avec les équipes vous avez en plus appris que les données des merchants et vouchers ne changent que rarement une fois configurés.

Plusieurs tâches vous sont confiées par votre Lead dév :
* Optimisez au maximum les performances de cette API sans introduire de nouvelles librairies ou technologies
* Faire une série de recommandations pour aller plus loin dans cette optimisation sans restriction de librairies ou technologies
* Refactor le code de la fonction MerchantController::findForPublicWebsite et de ses dépendances pour le rendre plus maintenable

En bonus :
* Afficher uniquement les merchants qui correspondent aux filtres suivants :
    * créé depuis moins d'un an
    * possède une valeur de cashbackFixedValue ou de cashbackPercentValue
    * possède au moins un voucher :
      * actif (status = active)
      * public (isPublic = true)
      * dont le jour courant est dans la période d'activité (jour courant compris entre dateStart et dateEnd inclus) 